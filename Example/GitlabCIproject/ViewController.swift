//
//  ViewController.swift
//  GitlabCIproject
//
//  Created by XYGDeveloper on 08/06/2021.
//  Copyright (c) 2021 XYGDeveloper. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  lazy var button: UIButton = {
    let btn = UIButton.init(
      frame: CGRect(
        x: 0, y: 0, width: UIScreen.main.bounds.size.width,
        height: UIScreen.main.bounds.size.height / 6))
    btn.backgroundColor = UIColor.red
    btn.addTarget(self, action: #selector(jump(sender:)), for: .touchUpInside)
    return btn
  }()
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.addSubview(button)
    self.view.backgroundColor = UIColor.blue
    // Do any additional setup after loading the view, typically from a nib.

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}

////

////

extension ViewController {

  @objc func jump(sender: UIButton) {
    print("11111111")
  }
}
