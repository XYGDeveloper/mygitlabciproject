# GitlabCIproject

[![CI Status](https://img.shields.io/travis/XYGDeveloper/GitlabCIproject.svg?style=flat)](https://travis-ci.org/XYGDeveloper/GitlabCIproject)
[![Version](https://img.shields.io/cocoapods/v/GitlabCIproject.svg?style=flat)](https://cocoapods.org/pods/GitlabCIproject)
[![License](https://img.shields.io/cocoapods/l/GitlabCIproject.svg?style=flat)](https://cocoapods.org/pods/GitlabCIproject)
[![Platform](https://img.shields.io/cocoapods/p/GitlabCIproject.svg?style=flat)](https://cocoapods.org/pods/GitlabCIproject)

## Example

To run the example project, in root dir   run `bundle install` and
cd Example  run`bundle exec pod install` from the Example directory first.

## Requirements
`bundler` 
`cocopods` 
`pod lib create`
`fastlane`
`swift`
`swift-format`
`clang-format`
`git`
`CI/CD`
## Installation

GitlabCIproject is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GitlabCIproject'
```

## Author

XYGDeveloper, 15243228311@163.com

## License

GitlabCIproject is available under the MIT license. See the LICENSE file for more info.
